package uz.pdp.pcmarketdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.UserDto;
import uz.pdp.pcmarketdemo.service.UserService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @PreAuthorize(value = "hasRole('SUPER_ADMIN')")
    @GetMapping("/getAllUsers")
   public HttpEntity<ApiResponse> getAllUsers(){
        ApiResponse allUsers = userService.getAllUsers();
        return ResponseEntity.status(200).body(allUsers);
    }


    @PreAuthorize(value = "hasRole('SUPER_ADMIN')")
    @GetMapping("/getUserById/{id}")
    public HttpEntity<ApiResponse> getUserById(@PathVariable Integer id){
        ApiResponse apiResponse = userService.getUserById(id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(200).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }


    @PreAuthorize(value = "hasRole('SUPER_ADMIN')")
    @GetMapping("/getPageableUsers")
    public HttpEntity<ApiResponse> getPageableUsers(@RequestParam int page, int size){
        ApiResponse apiResponse = userService.getPageableUsers(page, size);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(HttpStatus.OK).body(apiResponse);
        }else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(apiResponse);
        }
    }

    @PreAuthorize(value = "hasRole('SUPER_ADMIN')")
    @DeleteMapping("/deleteUser/{id}")
    public HttpEntity<ApiResponse> deleteUser(@PathVariable Integer id){
        ApiResponse apiResponse = userService.deleteUser(id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(apiResponse);
        }else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(apiResponse);
        }
    }

    @PreAuthorize(value = "hasRole('SUPER_ADMIN')")
    @PostMapping("/saveUser")
    public HttpEntity<ApiResponse> saveUser(@Valid @RequestBody UserDto userDto){
        ApiResponse apiResponse = userService.saveNewUser(userDto);
        if (apiResponse.isSuccess()) {
            return ResponseEntity.status(201).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }

    @PreAuthorize(value = "hasRole('SUPER_ADMIN')")
    @PutMapping("/editUser/{id}")
    public HttpEntity<ApiResponse> editUser(@PathVariable Integer id, @Valid @RequestBody UserDto userDto){
        ApiResponse apiResponse = userService.editUser(userDto, id);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(200).body(apiResponse);
        }else {
            return ResponseEntity.status(409).body(apiResponse);
        }
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


}
