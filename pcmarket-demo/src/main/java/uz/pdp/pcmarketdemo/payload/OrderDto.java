package uz.pdp.pcmarketdemo.payload;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
public class OrderDto {
    @NotNull(message = "clientId bo`sh bo`lmasligi kerak")
    private Integer clientId;
    @NotNull(message = "clientId bo`sh bo`lmasligi kerak")
    private double totalPrice;
    @NotNull(message = "orderNote bo`sh bo`lmasligi kerak")
    private String orderNote;
    @NotNull(message = "Date bo`sh bo`lmasligi kerak")
    private Date date;
    @NotNull(message = "delivered bo`sh bo`lmasligi kerak")
    private boolean delivred;


}
