package uz.pdp.pcmarketdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private Client client;
    @Column(nullable = false)
    private double totalPrice;
    @Column(nullable = false)
    private String orderNote;
    @Column(nullable = false)
    private Date date;
    @Column(nullable = false)
    private boolean delivred;
}
