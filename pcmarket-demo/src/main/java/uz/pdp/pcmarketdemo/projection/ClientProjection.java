package uz.pdp.pcmarketdemo.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.pcmarketdemo.entity.Client;

@Projection(name = "clientProjection",types = Client.class)
public interface ClientProjection {
    Integer getId();
    String getName();
    String getAddress();
    String getPhoneNumber();
    String getEmail();
}
