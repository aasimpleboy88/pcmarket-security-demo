package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.pcmarketdemo.entity.Client;
import uz.pdp.pcmarketdemo.projection.ClientProjection;

@RepositoryRestResource(path = "client",collectionResourceRel = "listClients",excerptProjection = ClientProjection.class)
public interface ClientRepository extends JpaRepository<Client,Integer> {

}
