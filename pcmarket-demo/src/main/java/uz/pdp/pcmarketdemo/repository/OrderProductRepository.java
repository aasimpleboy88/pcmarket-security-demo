package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.pcmarketdemo.entity.OrderProducts;

public interface OrderProductRepository extends JpaRepository<OrderProducts,Integer> {

}
