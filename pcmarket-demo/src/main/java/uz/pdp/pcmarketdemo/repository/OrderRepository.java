package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.pcmarketdemo.entity.Order;

public interface OrderRepository extends JpaRepository<Order,Integer> {


}
