package uz.pdp.pcmarketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.pcmarketdemo.entity.Category;

public interface CategoryRepository extends JpaRepository<Category,Integer> {

}
