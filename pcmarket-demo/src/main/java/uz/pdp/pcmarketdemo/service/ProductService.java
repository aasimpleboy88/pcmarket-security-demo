package uz.pdp.pcmarketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import uz.pdp.pcmarketdemo.entity.Attachment;
import uz.pdp.pcmarketdemo.entity.Category;
import uz.pdp.pcmarketdemo.entity.Product;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.ProductDto;
import uz.pdp.pcmarketdemo.repository.AttachmentRepository;
import uz.pdp.pcmarketdemo.repository.CategoryRepository;
import uz.pdp.pcmarketdemo.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    CategoryRepository categoryRepository;

    public ApiResponse getAllProduct(){
        List<Product> productList = productRepository.findAll();
        return new ApiResponse("OK! Barcha productlar ro`yxati",true,productList);
    }

    public ApiResponse getProductById(Integer id){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()){
            Product product = optionalProduct.get();
            return new ApiResponse(id+"-ID dagi product",true,product);
        }else {
            return new ApiResponse(id+"-ID dagi product mavjud emas",false);
        }
    }

    public ApiResponse getPageableProduct(int page,int size){
        Pageable pageable= PageRequest.of(page, size);
        Page<Product> productPage = productRepository.findAll(pageable);
        return new ApiResponse(page+"-pagedagi productlar ro`yxati",true,productPage);
    }

    public ApiResponse deleteProduct(Integer id){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()){
            productRepository.deleteById(id);
            return new ApiResponse(id+"-ID dagi product o`chirildi",true);
        }else {
            return new ApiResponse(id+"-ID dagi product mavjud emas",false);
        }
    }

    public ApiResponse saveNewProduct(ProductDto productDto){
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(productDto.getAttachmentId());
        if (optionalAttachment.isPresent()){
            Optional<Category> optionalCategory = categoryRepository.findById(productDto.getCategoryId());
            if (optionalCategory.isPresent()){
                Product product=new Product();
                Attachment attachment = optionalAttachment.get();
                product.setAttachment(attachment);
                Category category = optionalCategory.get();
                product.setCategory(category);
                product.setDescription(productDto.getDescription());
                product.setPrice(productDto.getPrice());
                product.setInfo(productDto.getInfo());
                product.setName(productDto.getName());
                productRepository.save(product);
                return new ApiResponse("Product saqlandi",true);
            }else {
                return new ApiResponse(productDto.getCategoryId()+"-ID dagi category mavjud emas",false);
            }
        }else {
            return new ApiResponse(productDto.getAttachmentId()+"-ID dagi attechment mavjud emas",false);
        }
    }

    public ApiResponse editProduct(Integer id, ProductDto productDto){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()){
            Optional<Attachment> optionalAttachment = attachmentRepository.findById(productDto.getAttachmentId());
            if (optionalAttachment.isPresent()){
                Optional<Category> optionalCategory = categoryRepository.findById(productDto.getCategoryId());
                if (optionalCategory.isPresent()){
                    Product product = optionalProduct.get();
                    product.setName(productDto.getName());
                    product.setDescription(product.getDescription());
                    product.setPrice(productDto.getPrice());
                    product.setInfo(productDto.getInfo());
                    Attachment attachment = optionalAttachment.get();
                    product.setAttachment(attachment);
                    Category category = optionalCategory.get();
                    product.setCategory(category);
                    productRepository.save(product);
                    return new ApiResponse(id+"-ID dagi Product ma`lumotlari o`zgartirildi",true);
                }else {
                    return new ApiResponse(productDto.getCategoryId()+"-ID dagi Category mavjud emas",false);
                }

            }else {
                return new ApiResponse(productDto.getAttachmentId()+"-ID dagi Attachment mavjud emas",false);
            }
        }else {
            return new ApiResponse(id+"-ID dagi Product mavjud emas",false);
        }
    }


}
