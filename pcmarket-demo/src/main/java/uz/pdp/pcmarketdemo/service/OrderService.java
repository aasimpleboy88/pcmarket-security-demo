package uz.pdp.pcmarketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.pcmarketdemo.entity.Client;
import uz.pdp.pcmarketdemo.entity.Order;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.OrderDto;
import uz.pdp.pcmarketdemo.repository.ClientRepository;
import uz.pdp.pcmarketdemo.repository.OrderRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ClientRepository clientRepository;


    public ApiResponse getAllOrders(){
        List<Order> orderList = orderRepository.findAll();
        return new ApiResponse("Barcha orderlar ro`yxati",true,orderList);
    }

    public ApiResponse getOrderById(Integer id){
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()){
            Order order = optionalOrder.get();
            return new ApiResponse("OK! "+id+"-ID dagi order",true,order);
        }else {
            return new ApiResponse("ERROR! "+id+"-ID lik order mavjud emas",false);
        }
    }

    public ApiResponse getPageableOrder(int page,int size){
        Pageable pageable= PageRequest.of(page,size);
        Page<Order> orderPage = orderRepository.findAll(pageable);
        if (orderPage.isEmpty()){
            return new ApiResponse("ERROR! Xozircha orderlar mavjud emas",false);
        }else {
            return new ApiResponse("OK! Orderlar page",true,orderPage);
        }
    }

    public ApiResponse deleteOrder(Integer id){
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()){
            orderRepository.deleteById(id);
            return new ApiResponse("OK! "+id+"-ID dagi Order muvaffaqiyatli o`chirildi",true);
        }else {
            return new ApiResponse("ERROR!"+id+"-ID lik order mavjud emas",false);
        }
    }

    public ApiResponse saveNewOrder(OrderDto orderDto){
        Optional<Client> optionalClient = clientRepository.findById(orderDto.getClientId());
        if (optionalClient.isPresent()){
            Order order=new Order();
            order.setOrderNote(orderDto.getOrderNote());
            order.setDelivred(orderDto.isDelivred());
            order.setTotalPrice(orderDto.getTotalPrice());
            order.setDate(orderDto.getDate());
            Client client = optionalClient.get();
            order.setClient(client);
            orderRepository.save(order);
            return new ApiResponse("OK! Yangi order saqlandi",true);
        }else {
            return new ApiResponse("ERROR! Client Id xato kiritildi",false);
        }
    }

    public ApiResponse editOrder(Integer id, OrderDto orderDto){
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()){
            Optional<Client> optionalClient = clientRepository.findById(orderDto.getClientId());
            if (optionalClient.isPresent()){
                Order order = optionalOrder.get();
                order.setOrderNote(orderDto.getOrderNote());
                order.setDelivred(orderDto.isDelivred());
                order.setTotalPrice(orderDto.getTotalPrice());
                order.setDate(orderDto.getDate());
                Client client = optionalClient.get();
                order.setClient(client);
                orderRepository.save(order);
                return new ApiResponse("OK! Order ma`lumotlari muvaffaqiyatli o`zgartirildi",true);
            }else {
                return new ApiResponse("ERROR! Client ID xato kiritildi",false);
            }
        }else {
            return new ApiResponse("ERROR! Order ID xato kiritildi",false);
        }
    }

}
