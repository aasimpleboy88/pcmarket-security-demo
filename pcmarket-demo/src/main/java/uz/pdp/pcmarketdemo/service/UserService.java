package uz.pdp.pcmarketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.pdp.pcmarketdemo.entity.User;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.payload.UserDto;
import uz.pdp.pcmarketdemo.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public ApiResponse getAllUsers(){
        List<User> allUsers = userRepository.findAll();
        return new ApiResponse("OK! Barcha userlar ro`yxati",true);
    }

    public ApiResponse getUserById(Integer id){
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            return new ApiResponse("OK! "+id+"-IDli user ma`lumotlari",true);
        }else {
            return new ApiResponse("ERROR! "+id+"-IDli user mavjud emas",false);
        }
    }

    public ApiResponse getPageableUsers(int page, int size){
        Pageable pageable= PageRequest.of(page,size);
        Page<User> userPage = userRepository.findAll(pageable);
        if (!userPage.isEmpty()){
            return new ApiResponse("OK! Userlarning page",true);
        }else {
            return new ApiResponse("ERROR! Users not found",false);
        }
    }

    public ApiResponse deleteUser(Integer id){
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            userRepository.deleteById(id);
            return new ApiResponse("OK! User o`chirildi",true);
        }else {
            return new ApiResponse("ERROR! Bunday user DB da mavjud emas",false);
        }
    }

    public ApiResponse saveNewUser(UserDto userDto){
        boolean existsByLogin = userRepository.existsByLogin(userDto.getLogin());
        if (existsByLogin){
            return new ApiResponse("ERROR! "+userDto.getLogin()+" loginlik user DB da mavjud, boshqa login tanlang",false);
        }else {
            boolean existsByEmail = userRepository.existsByEmail(userDto.getEmail());
            if (existsByEmail){
                return new ApiResponse("ERROR! "+userDto.getEmail()+" emaillik user DB da mavjud, boshqa email tanlang",false);
            }else {
                User user=new User();
                user.setName(userDto.getName());
                user.setRoleName(userDto.getRoleName());
                user.setLogin(userDto.getLogin());
                user.setPassword(userDto.getPassword());
                user.setEmail(userDto.getEmail());
                userRepository.save(user);
                return new ApiResponse("OK! User saqlandi",true);
            }
        }
    }

    public ApiResponse editUser(UserDto userDto, Integer id){
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()){
            boolean byLoginAndIdNot = userRepository.existsByLoginAndIdNot(userDto.getLogin(), id);
            if (byLoginAndIdNot){
                return new ApiResponse("ERROR! "+userDto.getLogin()+" ushbu loginlik user DBda mavjud",false);
            }else {
                boolean byEmailAndIdNot = userRepository.existsByEmailAndIdNot(userDto.getEmail(), id);
                if (byEmailAndIdNot){
                    return new ApiResponse("ERROR! "+userDto.getEmail()+ " ushbu emaillik user DB da mavjud",false);
                }else {
                    User user = optionalUser.get();
                    user.setName(userDto.getName());
                    user.setRoleName(userDto.getRoleName());
                    user.setLogin(userDto.getLogin());
                    user.setPassword(userDto.getPassword());
                    user.setEmail(userDto.getEmail());
                    userRepository.save(user);
                    return new ApiResponse("OK! User ma`lumotlari muvaffaqiyatli o`zgartirildi",true);
                }
            }
        }else {
            return new ApiResponse("ERROR! Bunday user DBda mavjud emas",false);
        }

    }



}
