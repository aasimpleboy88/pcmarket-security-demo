package uz.pdp.pcmarketdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.pcmarketdemo.entity.Attachment;
import uz.pdp.pcmarketdemo.entity.AttachmentContent;
import uz.pdp.pcmarketdemo.payload.ApiResponse;
import uz.pdp.pcmarketdemo.repository.AttachmentContentRepository;
import uz.pdp.pcmarketdemo.repository.AttachmentRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Autowired
    AttachmentContentService attachmentContentService;

    public ApiResponse getAllAttachment(){
        List<Attachment> attachmentList = attachmentRepository.findAll();
        return new ApiResponse("Barcha Attachment ro`yxati",true,attachmentList);
    }

    public ApiResponse getAttachmentById(Integer id){
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);
        if (optionalAttachment.isPresent()){
            Attachment attachment = optionalAttachment.get();
            return new ApiResponse(id+"-ID dagi Attachment",true,attachment);
        }else {
            return new ApiResponse(id+"-ID dagi Attachment mavjud emas",false);
        }
    }

    public ApiResponse getPageableAttachment(int page,int size){
        Pageable pageable= PageRequest.of(page, size);
        Page<Attachment> attachmentPage = attachmentRepository.findAll(pageable);
        return new ApiResponse(page+"-pagedagi Attachmentlar",true,attachmentPage);
    }

    public ApiResponse deleteAttachment(Integer id){
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);
        if (optionalAttachment.isPresent()){
            AttachmentContent byAttachmentId = attachmentContentRepository.findByAttachmentId(id);
            if (byAttachmentId!=null){
                attachmentContentRepository.deleteById(byAttachmentId.getId());
            }
            attachmentRepository.deleteById(id);
            return new ApiResponse(id+"-Id dagi attachment o`chirildi",true);
        }else {
            return new ApiResponse(id+"-ID dagi attachment mavjud emas",false);
        }
    }

    public ApiResponse uploadFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
        if (file!=null){
            String originalFilename = file.getOriginalFilename();
            long size = file.getSize();
            String contentType = file.getContentType();
            Attachment attachment=new Attachment();
            attachment.setOriginalName(originalFilename);
            attachment.setSize(size);
            attachment.setContentType(contentType);
            Attachment savedAttachment = attachmentRepository.save(attachment);

            ApiResponse apiResponse = attachmentContentService.uploadAttachmentContent(file, savedAttachment);
            if (apiResponse.isSuccess()){
                return new ApiResponse(originalFilename+" nomli fayl DB ga saqlandi",true);
            }else {
                return new ApiResponse("ERROR! "+originalFilename+" nomli fayl DBga saqlanmadi",false);
            }
        }else {
            return new ApiResponse("ERROR! Bo`sh fayl jo`natdingiz",false);
        }
    }

    public void downloadAttachment(Integer id, HttpServletResponse response) throws IOException {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);
        if (optionalAttachment.isPresent()){
            AttachmentContent byAttachmentId = attachmentContentRepository.findByAttachmentId(id);
            if (byAttachmentId!=null){
                Attachment attachment = optionalAttachment.get();
                response.setHeader("Content-Disposition", "attachment; filename=\""+attachment.getOriginalName()+"\"");
                response.setContentType(response.getContentType());
                FileCopyUtils.copy(byAttachmentId.getBytes(),response.getOutputStream());
            }

        }
    }

}
